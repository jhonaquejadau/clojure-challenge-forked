(ns challenge_third.invoice-item)

(defn- discount-factor [{:invoice-item/keys [discount-rate]
                         :or                {discount-rate 0}}]
  (- 1 (/ discount-rate 100.0)))
;(println (discount-factor {:invoice-item/discount-rate 20}))
(defn subtotal
  [{:invoice-item/keys [precise-quantity precise-price discount-rate]
    :as                item
    :or                {discount-rate 0}}]
  (* precise-price precise-quantity (discount-factor item)))
;{:invoice-item/precise-quantity 1.0 :invoice-item/precise-price 1000.0 :invoice-item/discount-rate 20}
;(println (subtotal {:invoice-item/precise-quantity 1 :invoice-item/precise-price 1000 :invoice-item/discount-rate 0}))
;(println (mapv subtotal [{:invoice-item/precise-quantity 2.0 :invoice-item/precise-price 200.0 :invoice-item/discount-rate 0}
;                         {:invoice-item/precise-quantity 3.0 :invoice-item/precise-price 1000.0 :invoice-item/discount-rate 40}
;                         {:invoice-item/precise-quantity 4.0 :invoice-item/precise-price 10.0 :invoice-item/discount-rate 0}
;                         {:invoice-item/precise-quantity 3.0 :invoice-item/precise-price 10000.0 :invoice-item/discount-rate 60}]))
