(ns challenge_second.invoice-spec
  (:require [clojure.spec.alpha :as s])
  (:require [clojure.data.json :as json])
  (:import (java.text SimpleDateFormat))
  )

(s/def :customer/name string?)
(s/def :customer/email string?)
(s/def :invoice/customer (s/keys :req [:customer/name
                                       :customer/email]))

(s/def :tax/rate double?)
(s/def :tax/category #{:iva})
(s/def ::tax (s/keys :req [:tax/category
                           :tax/rate]))
(s/def :invoice-item/taxes (s/coll-of ::tax :kind vector? :min-count 1))

(s/def :invoice-item/price double?)
(s/def :invoice-item/quantity double?)
(s/def :invoice-item/sku string?)

(s/def ::invoice-item
  (s/keys :req [:invoice-item/price
                :invoice-item/quantity
                :invoice-item/sku
                :invoice-item/taxes]))

(s/def :invoice/issue-date inst?)
(s/def :invoice/items (s/coll-of ::invoice-item :kind vector? :min-count 1))

(s/def ::invoice
  (s/keys :req [:invoice/issue-date
                :invoice/customer
                :invoice/items]))


;MY ANSWER ABOUT SPEC REQUIREMENT DESCRIBED IN THE CHALLENGE #2
(def invoice_json (:invoice (json/read-json (slurp "invoice.json"))))
(defn convert-str-to-date [value]
  (.parse (SimpleDateFormat. "MM/dd/yyyy") value)
  )
(defn mapping_tax [item_tax]
  {:tax/category :iva :tax/rate (double (:tax_rate item_tax))}
  )
;(def tax (map map-taxes (:taxes item)))
(defn converting_items [item]
  {
   :invoice-item/price (:price item)
   :invoice-item/quantity (:quantity item)
   :invoice-item/sku (:sku item)
   :invoice-item/taxes (mapv mapping_tax (:taxes item))
   }
  )

(defn invoice-json-to-map [item]
  {
   :invoice/issue-date (convert-str-to-date (:issue_date item))
   :invoice/customer {
                      :customer/name (:company_name (:customer item))
                      :customer/email (:email (:customer item))
                      }
   :invoice/items (mapv converting_items (:items invoice_json))
   }
  )

(println "NEW INVOICE GENERATED (edn file)")
(println "")
(clojure.pprint/pprint (invoice-json-to-map invoice_json))
(def invoice (invoice-json-to-map invoice_json))
(println "EXPLAIN SPEC METHOD")
(s/explain ::invoice invoice)
(println "VALIDATION OF THE NEW INVOICE CLOJURE MAP")
;(s/valid? ::invoice invoice)
(println (s/valid? ::invoice invoice))

;THIS LINES OF CODE BELOW WAS PART OF MULTIPLES TEST MADE TO VALIDATE EACH STEP... :invoice/customer, ::invoice-item, :invoice/items, ::invoice
;[
; {:invoice-item/price 100.00 :invoice-item/quantity 2.0 :invoice-item/sku "SUPER-1" :invoice-item/taxes [{:tax/category :iva :tax/rate (double 19)} {:tax/category :iva :tax/rate (double 19)} {:tax/category :iva :tax/rate (double 19)}]}
; {:invoice-item/price 100.00 :invoice-item/quantity 2.0 :invoice-item/sku "SUPER-1" :invoice-item/taxes [{:tax/category :iva :tax/rate (double 19)} {:tax/category :iva :tax/rate (double 19)} {:tax/category :iva :tax/rate (double 19)}]}
; {:invoice-item/price 100.00 :invoice-item/quantity 2.0 :invoice-item/sku "SUPER-1" :invoice-item/taxes [{:tax/category :iva :tax/rate (double 19)} {:tax/category :iva :tax/rate (double 19)} {:tax/category :iva :tax/rate (double 19)}]}
; ]
;(def customer {:customer/name "jhon" :customer/email "jjahs@gmail.com"})
""
;(s/explain :invoice/customer customer)

;(def tax [{:tax/category :iva :tax/rate (double 19)} {:tax/category :iva :tax/rate (double 19)} {:tax/category :iva :tax/rate (double 19)}])
;(s/explain :invoice-item/taxes tax)
;(def item {:invoice-item/price 100.00 :invoice-item/quantity 2.0 :invoice-item/sku "SUPER-1" :invoice-item/taxes tax})
;(clojure.pprint/pprint item)
;(s/explain ::invoice-item item)
;(def items-col [item item item])
;(s/explain :invoice/items items-col)

;(def date "13/10/2020")

;(println (s/valid? inst? (convert-str-to-date date)))

;(def inv {:invoice/issue-date (convert-str-to-date date) :invoice/customer customer :invoice/items items-col})
;(s/explain ::invoice inv)