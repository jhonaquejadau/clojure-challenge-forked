(ns challenge_one.challenge-one
  (:require [clojure.data.json :as json])
  )

(def invoice (json/read-str (json/write-str (clojure.edn/read-string (slurp "invoice.edn"))) :key-fn keyword))
;(clojure.pprint/pprint invoice)
(defn has-iva? [item]
  (and (= (:category item) "iva") (= (:rate item) 19))
  )
(defn has-retention? [item]
  (and (= (:category item) "ret_fuente") (= (:rate item) 1))
  )
(defn has-both? [item]
  (not= (and (contains? item :taxes) (= (some has-iva? (:taxes item)) true)) (and (contains? item :retentions) (= (some has-retention? (:retentions item)) true)))
  )

; (TESTING INVOICE)
; Variable invoice_test below, represents a custom invoice with the purposes of test items function, which is in charge to return all items that satisfy conditions described in the challenge.
(def invoice_test {
                   :id "test_i1"
                   :items [{:id "ii1",
                    :sku "SKU 1",
                    :taxes [{:id "t1", :category "iva", :rate 19} {:id "t1", :category "iva", :rate 19}],
                    :retentions [{:id "r1", :category "ret_fuente", :rate 1} {:id "r1", :category "ret_fuente", :rate 2}]}
                   {:id "ii2",
                    :sku "SKU 2",
                    :taxes [{:id "t1", :category "iva", :rate 19} {:id "t2", :category "ia", :rate 1}{:id "t3", :category "iva", :rate 19}],
                    :retentions [{:id "r1", :category "ret_fuente", :rate 3} {:id "r2", :category "ret_fuente", :rate 2}]}
                   {:id "ii3",
                    :sku "SKU 2",
                    :taxes [{:id "t2", :category "iva", :rate 16}]}
                   {:id "ii4",
                    :sku "SKU 3",
                    :taxes [{:id "t3", :category "iva", :rate 19}]}
                   {:id "ii5",
                    :sku "SKU 3",
                    :retentions [{:id "r2", :category "ret_fuente", :rate 1}{:id "r2", :category "ret_fuente", :rate 3}]}
                   {:id "ii6",
                    :sku "SKU 4",
                    :retentions [{:id "r3", :category "ret_fuente", :rate 2} {:id "r3", :category "ret_fuente", :rate 1}]}
                   ]})

;Uncomment (clojure.pprint/pprint) line if you can probe this items invoice with the function to verify if an ite has (:taxes & :retentions) and also, if satify (iva = 19 & ret_fuente = 1)
;Note: Look that invice_test has many items inside :taxes or :retentions....

;(clojure.pprint/pprint (filterv has-both? (:items invoice_test)))


;FUNCTION THAT SATISFY CONDITION USING THEAD-LAST OPERATOR
(defn items [invoice]
  (println "ORIGINAL INVOICE")
  (println invoice)
  (println "-----------------------------------------------------------------------")
  (println "ITEMS FILTERED -- REQUIREMENTS: SOME ITEM THAT HAS AT LEAST IVA = 19% OR RETENTION = 1% & HAS NOT BOTH OF THEM")
  (->> (filterv has-both? (:items invoice))
       )
  )
;USING INVOICE OF THE CALLENGE (invoice)
(clojure.pprint/pprint (items invoice))

;USING CUSTOM INVOICE (invoice_test)
;(clojure.pprint/pprint (items invoice_test))
