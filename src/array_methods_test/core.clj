(ns array_methods_test.core)

;THIS FILE IS JUST FOR PRACTICE ARRAY METHODS
(def j [
        {:name "jhon" :age 24}
        {:name "danna" :age 18}
        ])
(defn is-true? [value] (= (:name value) "jhon"))
(println (filter is-true? j))
(println (map (partial :name) j))
(println (is-true? {:name "danna"}))