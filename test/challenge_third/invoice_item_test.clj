(ns challenge_third.invoice-item-test
  (:require [clojure.test :refer :all]
            [challenge_third.invoice-item :refer :all])
  )


(deftest subtotal-test
  (testing "No quantity - No price - No discount rate || Quantity or price value equal to 0.0"
    (is (= 0.0 (subtotal {:invoice-item/precise-quantity 0.0 :invoice-item/precise-price 0.0 :invoice-item/discount-rate 0})))
    (is (= 0.0 (subtotal {:invoice-item/precise-quantity 0.0 :invoice-item/precise-price 2560.0 :invoice-item/discount-rate 80})))
    )
  (testing "Valid amount -- Subtotal value is greater than 0"
    (is (< 0 (subtotal {:invoice-item/precise-quantity 1.0 :invoice-item/precise-price 1000.0 :invoice-item/discount-rate 60})))
    )
  (testing "Invalid amount -- Subtotal value is less than 0"
    (is (> 0 (subtotal {:invoice-item/precise-quantity 2.0 :invoice-item/precise-price -100.0 :invoice-item/discount-rate 20})))
    )
  (testing "There is no discount for the item, therefore price will be the same (quantity*price)"
    (is (= 1500.0 (subtotal {:invoice-item/precise-quantity 3.0 :invoice-item/precise-price 500.0 :invoice-item/discount-rate 0})))
    )
  (testing "Subtotal arguments are int but function return double"
    (is (= 1500.0 (subtotal {:invoice-item/precise-quantity 2 :invoice-item/precise-price 1500 :invoice-item/discount-rate 50})))
    )
  (testing "Discount rate of 100% element is free"
    (is (= 0.0 (subtotal {:invoice-item/precise-quantity 2 :invoice-item/precise-price 1500 :invoice-item/discount-rate 100})))
    )
  (testing "Some value inside subtotal vector are valid"
    (is (= true (some? (mapv subtotal [{:invoice-item/precise-quantity 2.0 :invoice-item/precise-price 200.0 :invoice-item/discount-rate 0}
                                       {:invoice-item/precise-quantity 3.0 :invoice-item/precise-price 1000.0 :invoice-item/discount-rate 40}
                                       {:invoice-item/precise-quantity 4.0 :invoice-item/precise-price 10.0 :invoice-item/discount-rate 0}
                                       {:invoice-item/precise-quantity 3.0 :invoice-item/precise-price 10000.0 :invoice-item/discount-rate 60}]))))
    )
  (testing "Items vector is not empty -- At least has 1 element"
    (is (< 1 (count (mapv subtotal [{:invoice-item/precise-quantity 2.0 :invoice-item/precise-price 200.0 :invoice-item/discount-rate 0}
                                    {:invoice-item/precise-quantity 3.0 :invoice-item/precise-price 1000.0 :invoice-item/discount-rate 40}
                                    {:invoice-item/precise-quantity 4.0 :invoice-item/precise-price 10.0 :invoice-item/discount-rate 0}
                                    {:invoice-item/precise-quantity 3.0 :invoice-item/precise-price 10000.0 :invoice-item/discount-rate 60}]))))
    )
  (testing "There is no elements inside items vector"
    (is (= 0 (count (mapv subtotal []))))
    )
  )

